package controllers

import models.FilialBranch
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.Future

object Application extends Controller {
	val index: (String) => Action[AnyContent] = findTopFilialBranchesByRating(Set("Новосибирск", "Омск", "Томск", "Кемерово", "Новокузнецк"))

	private def findTopFilialBranchesByRating(places: Set[String])(sphere: String): Action[AnyContent] =
		if(sphere == null || sphere.isEmpty) Action(Ok(JsString("Sphere not specified")))
		else Action.async {
			implicit val context = scala.concurrent.ExecutionContext.Implicits.global

			val ratingMinScale = 1

			implicit val writes = new Writes[FilialBranch] {
				override def writes(branch: FilialBranch): JsValue = JsObject(
					Seq(
						"name" -> JsString(branch.name),
						"address" -> JsString(branch.cityName + ", " + branch.localAddress))
					  ++ branch.rating.map(rating => {
						val scaledRating: BigDecimal = if (rating.scale >= ratingMinScale) rating else rating.setScale(ratingMinScale)
						"rating" -> JsString(scaledRating.toString())
					}))
			}

			val findInCity: (String) => Future[Option[FilialBranch]] = FilialBranch.findTopByRating(sphere)

			val futureBranches: Future[Set[Option[FilialBranch]]] = Future.traverse(places)(city => findInCity(city))

			val defaultRating: BigDecimal = 0

			futureBranches.map(results => {
				val sortedBranches: List[FilialBranch] = results.flatten.toList.sortWith(
					_.rating.getOrElse(defaultRating) > _.rating.getOrElse(defaultRating))
				Ok(Json.toJson(sortedBranches))
			})
	}
}
