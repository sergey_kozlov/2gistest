package models

import models.api2gis.Api2gis
import play.api.Play.current
import play.api.libs.json._
import play.api.libs.ws._

import scala.concurrent.Future

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 09.07.2014
 */
case class FilialBranch(
						 id: String,
						 hash: String,
						 name: String,
						 cityName: String,
						 localAddress: String,
						 rating: Option[BigDecimal] = None)

object FilialBranch{
	def findTopByRating(sphere: String)(where: String): Future[Option[FilialBranch]] = {
		implicit val context = scala.concurrent.ExecutionContext.Implicits.global

		implicit val reads = new Reads[FilialBranch] {
			override def reads(json: JsValue): JsResult[FilialBranch] = JsSuccess(FilialBranch(
				id = (json \ "id").as[String],
				hash = (json \ "hash").as[String],
				name = (json \ "name").as[String],
				cityName = (json \ "city_name").as[String],
				localAddress = (json \ "address").as[String]))
		}

		for{
			bestBranchesResponse: WSResponse <- WS.url("http://catalog.api.2gis.ru/search").withQueryString(
				"key" -> Api2gis.Key,
				"version" -> Api2gis.Version,
				"what" -> sphere,
				"where" -> where,
				"sort" -> "rating",
				"pagesize" -> "5").get()
			result <- {
				if((bestBranchesResponse.json \ "response_code").as[String] == "404") Future(None)
				else {
					lazy val branch: FilialBranch = (bestBranchesResponse.json \ "result").as[List[FilialBranch]].head
					WS.url("http://catalog.api.2gis.ru/profile").withQueryString(
						"key" -> Api2gis.Key,
						"version" -> Api2gis.Version,
						"id" -> branch.id,
						"hash" -> branch.hash).get().map { response =>
						Some(branch.copy(rating = (response.json \ "rating").asOpt[BigDecimal]))
					}
				}
			}
		} yield result
	}

}