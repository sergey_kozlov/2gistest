package models.api2gis

import play.api.Configuration
import play.api.Play.current

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 11.07.2014
 * Time: 12:54
 */

object Api2gis{
	private val Config: Configuration = current.configuration.getConfig("2gis").get

	val Key: String = Config.getString("key").get
	val Version: String = Config.getString("version").get
}